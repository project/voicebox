;Make file for the Voicebox install profile
; $Id: voicebox.make 575 2011-04-11 20:25:21Z jeff $
; version = 6.x-1.0beta2
api = 2
core = 6.20

;Core
projects[] = drupal

;Contrib
projects[auto_nodetitle][version] = 1.2
projects[auto_nodetitle][subdir] = "contrib"
projects[better_formats][version] = 1.2
projects[better_formats][subdir] = "contrib"
projects[calendar][version] = 2.4
projects[calendar][subdir] = "contrib"
projects[content_profile][version] = 1.0
projects[content_profile][subdir] = "contrib"
projects[ctools][version] = 1.8
projects[ctools][subdir] = "contrib"
projects[cck][version] = 2.9
projects[cck][subdir] = "contrib"
projects[date][version] = 2.7
projects[date][subdir] = "contrib"
projects[diff][version] = 2.1
projects[diff][subdir] = "contrib"
projects[filefield][version] = 3.9
projects[filefield][subdir] = "contrib"
projects[flag][version] = 2.0-beta5
projects[flag][subdir] = "contrib"
projects[image_resize_filter][version] = 1.12
projects[image_resize_filter][subdir] = "contrib"
projects[imageapi][version] = 1.9
projects[imageapi][subdir] = "contrib"
projects[imagecache][version] = 2.0-beta10
projects[imagecache][subdir] = "contrib"
projects[imagecache_profiles][version] = 1.3
projects[imagecache_profiles][subdir] = "contrib"
projects[imagefield][version] = 3.9
projects[imagefield][subdir] = "contrib"
projects[insert][version] = 1.0
projects[insert][subdir] = "contrib"
projects[job_scheduler][version] = 1.0-beta3
projects[job_scheduler][subdir] = "contrib"
projects[jquery_ui][version] = 1.4
projects[jquery_ui][subdir] = "contrib"
projects[jquery_update][version] = 2.0-alpha1
projects[jquery_update][subdir] = "contrib"
projects[libraries][version] = 1.0
projects[libraries][subdir] = "contrib"
projects[link][version] = 2.9
projects[link][subdir] = "contrib"
projects[mailhandler][version] = 1.11
projects[mailhandler][subdir] = "contrib"
projects[mimerouter][version] = 1.0
projects[mimerouter][subdir] = "contrib"
projects[pathauto][version] = 1.5
projects[pathauto][subdir] = "contrib"
projects[skinr][version] = 1.6
projects[skinr][subdir] = "contrib"
projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"
projects[tagadelic][version] = 1.3
projects[tagadelic][subdir] = "contrib"
projects[thickbox][version] = 1.6
projects[thickbox][subdir] = "contrib"
projects[token][version] = 1.15
projects[token][subdir] = "contrib"
projects[transliteration][version] = 3.0
projects[transliteration][subdir] = "contrib"
projects[views][version] = 2.12
projects[views][subdir] = "contrib"
projects[views_bulk_operations][version] = 1.10
projects[views_bulk_operations][subdir] = "contrib"
projects[views_slideshow][version] = 2.3
projects[views_slideshow][subdir] = "contrib"
projects[tagadelic_views][version] = 1.2
projects[tagadelic_views][subdir] = "contrib"
projects[wysiwyg][version] = 2.3
projects[wysiwyg][subdir] = "contrib"
projects[swftools][version] = 3.0-beta5
projects[swftools][subdir] = "contrib"

projects[simpleshare][location] = http://code.developmentseed.org/fserver
projects[simpleshare][version] = 1.0-beta5
projects[simpleshare][subdir] = "contrib"

; see 
;   http://drupal.org/node/860974
;   http://drupal.org/node/927576
projects[features][version] = 1.0
projects[features][subdir] = "contrib"
projects[features][patch][] = "http://drupal.org/files/issues/860974_menu_links_customized.patch"

; see http://drupal.org/node/872564
projects[context][version] = 3.0
projects[context][subdir] = "contrib"
projects[context][patch][] = "http://drupal.org/files/issues/context-872564-26.patch"

; see 
;   http://drupal.org/node/723548
;   http://drupal.org/node/927888
projects[feeds][version] = 1.0-beta10
projects[feeds][subdir] = "contrib"
projects[feeds][patch][] = "http://drupal.org/files/issues/feeds-723548-30.patch"

; see
;   http://drupal.org/node/1027600
;   http://drupal.org/node/927020
projects[mimedetect][version] = 1.3
projects[mimedetect][subdir] = "contrib"
projects[mimedetect][patch][] = "http://drupal.org/files/issues/mimedetect-1027600-3.patch"

;Custom
projects[voicebox_views_helper][location] = http://code.funnymonkey.com/fserver
projects[voicebox_views_helper][subdir] = "voicebox"

;Features
projects[voicebox_bio][location] = http://code.funnymonkey.com/fserver
projects[voicebox_bio][subdir] = "voicebox/features"
projects[voicebox_bio][version] = 1.0-beta1

projects[voicebox_channels][location] = http://code.funnymonkey.com/fserver
projects[voicebox_channels][subdir] = "voicebox/features"
projects[voicebox_channels][version] = 1.0-beta1

projects[voicebox_core][location] = http://code.funnymonkey.com/fserver
projects[voicebox_core][subdir] = "voicebox/features"
projects[voicebox_core][version] = 1.0-beta1

projects[voicebox_discussion][location] = http://code.funnymonkey.com/fserver
projects[voicebox_discussion][subdir] = "voicebox/features"
projects[voicebox_discussion][version] = 1.0-beta1

projects[voicebox_homepage][location] = http://code.funnymonkey.com/fserver
projects[voicebox_homepage][subdir] = "voicebox/features"
projects[voicebox_homepage][version] = 1.0-beta1

projects[voicebox_imported_content][location] = http://code.funnymonkey.com/fserver
projects[voicebox_imported_content][subdir] = "voicebox/features"
projects[voicebox_imported_content][version] = 1.0-beta1

projects[voicebox_issue][location] = http://code.funnymonkey.com/fserver
projects[voicebox_issue][subdir] = "voicebox/features"
projects[voicebox_issue][version] = 1.0-beta1

projects[voicebox_maintainer][location] = http://code.funnymonkey.com/fserver
projects[voicebox_maintainer][subdir] = "voicebox/features"
projects[voicebox_maintainer][version] = 1.0-beta1

projects[voicebox_member_ui][location] = http://code.funnymonkey.com/fserver
projects[voicebox_member_ui][subdir] = "voicebox/features"
projects[voicebox_member_ui][version] = 1.0-beta1

projects[voicebox_navigation][location] = http://code.funnymonkey.com/fserver
projects[voicebox_navigation][subdir] = "voicebox/features"
projects[voicebox_navigation][version] = 1.0-beta1

projects[voicebox_posts][location] = http://code.funnymonkey.com/fserver
projects[voicebox_posts][subdir] = "voicebox/features"
projects[voicebox_posts][version] = 1.0-beta1

;Theme
projects[hexagon] = 1.5-beta3
projects[dispatch][location] = http://code.funnymonkey.com/fserver
projects[dispatch][version] = 1.0-beta4

;Voicebox install profile
projects[voicebox_profile][location] = http://code.funnymonkey.com/fserver
projects[voicebox_profile][version] = 1.0-beta1

;Libraries
libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://downloads.sourceforge.net/project/tinymce/TinyMCE/3.3.2/tinymce_3_3_2.zip"
libraries[tinymce][directory_name] = "tinymce"

libraries[audio-player][download][type] = "get"
libraries[audio-player][download][url] = "http://wpaudioplayer.com/wp-content/downloads/audio-player-standalone.zip"
libraries[audio-player][directory_name] = "audio-player"

libraries[flowplayer][download][type] = "get"
libraries[flowplayer][download][url] = "http://releases.flowplayer.org/flowplayer/flowplayer-3.2.2.zip"
libraries[flowplayer][directory_name] = "flowplayer3"

libraries[swfobject][download][type] = "get"
libraries[swfobject][download][url] = "http://swfobject.googlecode.com/files/swfobject_2_2.zip"
libraries[swfobject][directory_name] = "swfobject"

libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery-ui-1.7.3.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"
libraries[jquery_ui][destination] = "modules/contrib/jquery_ui"
